#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include "chart.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setColumnWidth(0, 15);
    ui->tableWidget->setColumnWidth(1, 80);
    ui->tableWidget->setColumnWidth(2, 50);
    ui->tableWidget->setColumnWidth(3, 140);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "№" << "Name" << "Mass" << "Conductivity");
    ui->tableWidget->verticalHeader()->setVisible(false);
}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_Write_Button_clicked()
{
    Dialog d;
    d.setModal(true);
    d.exec();
}
void MainWindow::on_Conductor_Button_clicked()
{

    ui->tableWidget->clear();
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "№" << "Name" << "Mass" << "Conductivity");
    int i = 0;
    std::ifstream ifs("data.dat");
    if (ifs.is_open()) {
        while (!ifs.eof()) {
            ifs >> element[i].number_ >> element[i].name_ >> element[i].mass_ >> element[i].conductivity_;
            if(element[i].conductivity_ == "Conductor")
                i++;
        }
    }
    ifs.close();
    i--;
    int size = i;
    Element pr;
    for (int f = 0; f < size - 1; f++)
    {
        for (int j = 0; j < size - f - 1; j++)
        {
            if (element[j].number_ > element[j + 1].number_)
            {
                pr = element[j];
                element[j] = element[j + 1];
                element[j + 1] = pr;
            }
        }
    }

    ui->tableWidget->setRowCount(i);
    for (int j = 0; j < i; j++) {
        if (element[j].conductivity_ == "Conductor")
        {
            for (int g = 0; g<4; g++)
            {
                if (g==0)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::number(element[j].number_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g==1)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::fromStdString(element[j].name_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g==2)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::number(element[j].mass_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g==3)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::fromStdString(element[j].conductivity_));
                    ui->tableWidget->setItem(j, g, itm);
                }

            }
        }

    }
}

void MainWindow::on_Semiconductor_Button_clicked()
{
    ui->tableWidget->clear();
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "№" << "Name" << "Mass" << "Conductivity");
    int i = 0;
    std::ifstream ifs("data.dat");
    if (ifs.is_open())
    {
        while (!ifs.eof())
        {
            ifs >> element[i].number_ >> element[i].name_ >> element[i].mass_ >> element[i].conductivity_;
            if(element[i].conductivity_ == "Semiconductor")
                i++;
        }
    }
    i--;
    int size = i;
    Element pr;
    for (int f = 0; f < size - 1; f++)
    {
        for (int j = 0; j < size - f - 1; j++)
        {
            if (element[j].number_ > element[j + 1].number_)
            {
                pr = element[j];
                element[j] = element[j + 1];
                element[j + 1] = pr;
            }
        }
    }

    ifs.close();
    ui->tableWidget->setRowCount(i);

    for (int j = 0; j < i; j++)
    {
        if (element[j].conductivity_ == "Semiconductor")
        {
            for (int g = 0; g < 4;g++)
            {
                if (g == 0) {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::number(element[j].number_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g == 1)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::fromStdString(element[j].name_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g == 2)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::number(element[j].mass_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g == 3)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::fromStdString(element[j].conductivity_));
                    ui->tableWidget->setItem(j, g, itm);
                }
            }
        }
    }
}

void MainWindow::on_Insulator_Button_clicked()
{
    ui->tableWidget->clear();
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "№" << "Name" << "Mass" << "Conductivity");
    int i =0;
    std::ifstream ifs("data.dat");
    if (ifs.is_open())
    {
        while (!ifs.eof())
        {
            ifs >> element[i].number_ >> element[i].name_ >> element[i].mass_ >> element[i].conductivity_;
            if(element[i].conductivity_ == "Isolator")
                i++;
        }
    }
    i--;

    ifs.close();

    int size = i;
    Element pr;

    for (int f = 0; f < size - 1; f++)
    {
        for (int j = 0; j < size - f - 1; j++)
        {
            if (element[j].number_ > element[j + 1].number_)
            {
                pr = element[j];
                element[j] = element[j + 1];
                element[j + 1] = pr;
            }
        }
    }
    ui->tableWidget->setRowCount(i);
    for (int j = 0; j < i; j++)
    {
        if (element[j].conductivity_ == "Isolator" && element[j].mass_ > ui->spinBox->value() )
        {
            for (int g = 0; g < 4; g++)
            {
                if (g == 0)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::number(element[j].number_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g == 1)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::fromStdString(element[j].name_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g == 2)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::number(element[j].mass_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g == 3)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::fromStdString(element[j].conductivity_));
                    ui->tableWidget->setItem(j, g, itm);
                }
            }
        }
    }
}

void MainWindow::on_Chart_Button_clicked()
{
    Chart chart;
    chart.setModal(true);
    chart.exec();
}

void MainWindow::on_All_Button_clicked()
{
    ui->tableWidget->clear();
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "№" << "Name" << "Mass" << "Conductivity");
    int i =0;
    std::ifstream ifs("data.dat");
    if (ifs.is_open())
    {
        while (!ifs.eof())
        {
            ifs >> element[i].number_ >> element[i].name_ >> element[i].mass_ >> element[i].conductivity_;
            i++;
        }
    }
    i--;

    ifs.close();

    int size = i;
    Element pr;

    for (int f = 0; f < size - 1; f++)
    {
        for (int j = 0; j < size - f - 1; j++)
        {
            if (element[j].number_ > element[j + 1].number_)
            {
                pr = element[j];
                element[j] = element[j + 1];
                element[j + 1] = pr;
            }
        }
    }
    ui->tableWidget->setRowCount(i);
    for (int j = 0; j < i; j++)
    {
        if (element[j].mass_ > ui->spinBox->value() )
        {
            for (int g = 0; g < 4; g++)
            {
                if (g == 0)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::number(element[j].number_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g == 1)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::fromStdString(element[j].name_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g == 2)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::number(element[j].mass_));
                    ui->tableWidget->setItem(j, g, itm);
                }
                else if (g == 3)
                {
                    QTableWidgetItem *itm = new QTableWidgetItem(QString::fromStdString(element[j].conductivity_));
                    ui->tableWidget->setItem(j, g, itm);
                }
            }
        }
    }
}
