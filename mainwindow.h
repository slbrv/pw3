#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <fstream>

struct Element {
    int number_;
    std::string name_;
    float mass_;
    std::string conductivity_;

};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private slots:
    void on_Write_Button_clicked();

    void on_Conductor_Button_clicked();
    void on_Semiconductor_Button_clicked();
    void on_Insulator_Button_clicked();

    void on_Chart_Button_clicked();

    void on_All_Button_clicked();

private:
    Ui::MainWindow *ui;
    Element element[120];
    Element pr;
};

#endif // MAINWINDOW_H
