#include "chart.h"
#include "ui_chart.h"
#include "mainwindow.h"

Chart::Chart(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Chart)
{
    ui->setupUi(this);
    Element element[120];
    int i =0;
    std::ifstream ifs("data.dat");
    if (ifs.is_open()) {
        while (!ifs.eof()) {
            ifs >> element[i].number_ >> element[i].name_ >> element[i].mass_ >> element[i].conductivity_;
            i++;
        }
    }
    i--;
    int size = i;
    Element pr;
    for (int f = 0; f < size - 1; f++) {
        for (int j = 0; j < size - f - 1; j++) {
            if (element[j].number_ > element[j + 1].number_) {
                pr = element[j];
                element[j] = element[j + 1];
                element[j + 1] = pr;
            }
        }
    }
    ifs.close();

    QChartView *chartView = new QChartView(this);
    ui->horizontalLayout->addWidget(chartView);

    QLineSeries *series = new QLineSeries();
    int k = 0;
    while (k < i)
    {
        *series << QPointF(element[k].number_, element[k].mass_);
        ++k;
    }
    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->legend()->hide();
    chart->setTitle("Graph of the specific weight of the element from its number");
    chart ->createDefaultAxes();
    chartView->setRenderHint(QPainter::Antialiasing);


    chartView->setChart(chart);

}

Chart::~Chart()
{
    delete ui;
}
