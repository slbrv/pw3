#ifndef CHART_H
#define CHART_H

#include <QDialog>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QValueAxis>
#include <math.h>

using namespace QtCharts;

namespace Ui {
class Chart;
}

class Chart : public QDialog
{
    Q_OBJECT

public:
    explicit Chart(QWidget *parent = nullptr);
    ~Chart();

private:
    Ui::Chart *ui;
    QChart *chart;
};

#endif // CHART_H
