#include "dialog.h"
#include "ui_dialog.h"
#include "mainwindow.h"
#include <QMessageBox>
#include <QRegExpValidator>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->lineEdit->setValidator(new QRegExpValidator(QRegExp("[0-9]*"), ui->lineEdit));
    ui->lineEdit_2->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z]*"), ui->lineEdit_2));
    ui->lineEdit_3->setValidator(new QRegExpValidator(QRegExp("[0-9]*.[0-9]*"), ui->lineEdit_3));
    ui->lineEdit_4->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z]*"), ui->lineEdit_4));
    ui->Add_Button->setEnabled(false);
    ui->Delete_Button->setEnabled(false);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_Add_Button_clicked()
{
    int k = ui->lineEdit->text().toInt();
    Element element[120];
    int i =0;
    std::ifstream ifs("data.dat");
    if (ifs.is_open())
    {
        while (!ifs.eof())
        {
            ifs >> element[i].number_ >> element[i].name_ >> element[i].mass_ >> element[i].conductivity_;
            i++;
        }
    }
    i--;
    ifs.close();
    bool key = true;

    for (int j = 0; j < i; j++)
        if (element[j].number_ == k)
            key = false;

    if (key)
    {
        std::ofstream ofs("data.dat", std::ios::app);
        el.number_ = ui->lineEdit->text().toInt();
        el.name_ = ui->lineEdit_2->text().toUtf8().data();
        el.mass_ = ui->lineEdit_3->text().toInt();
        el.conductivity_ = ui->lineEdit_4->text().toUtf8().data();
        if (ofs.is_open())
            ofs << el.number_ << " " << el.name_ << " " << el.mass_ << " " << el.conductivity_ << " ";
        ofs.close();
    }
    else
        QMessageBox::warning(this, "Warning!", "Item already exists!");
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    ui->lineEdit_3->clear();
    ui->lineEdit_4->clear();
}

void Dialog::check(){
    if ((ui->lineEdit->text() == "") || (ui->lineEdit_2->text() == "")
            || (ui->lineEdit_3->text() == "")|| (ui->lineEdit_4->text() == ""))
        ui->Add_Button->setEnabled(false);
    else
        ui->Add_Button->setEnabled(true);
}

void Dialog::on_lineEdit_textChanged(const QString &arg1)
{
    check();
    if (ui->lineEdit->text() == ""){
        ui->Delete_Button->setEnabled(false);
    }
    else {
        ui->Delete_Button->setEnabled(true);
    }

}

void Dialog::on_lineEdit_2_textChanged(const QString &arg1)
{
    check();
}

void Dialog::on_lineEdit_3_textChanged(const QString &arg1)
{
    check();
}

void Dialog::on_lineEdit_4_textChanged(const QString &arg1)
{
    check();
}

void Dialog::on_Delete_Button_clicked()
{
    int i = 0;
    std::ifstream ifs("data.dat");
    if (ifs.is_open())
    {
        while (!ifs.eof())
        {
            ifs >> element[i].number_ >> element[i].name_ >> element[i].mass_ >> element[i].conductivity_;
            i++;
        }
    }
    i--;
    ifs.close();
    int del = ui->lineEdit->text().toInt();
    int p =0;
    bool key = false;
    for (int j =0; j<i;j++) {
        if (del == element[j].number_) {
            p=j;
            key = true;
        }
    }
    if (key)
    {
        for (int j = p; j < i - 1; j++)
        {
            element[j] = element[j+1];
        }
        i--;
        std::ofstream ofs("data.dat");
        if (ofs.is_open())
            for (int j = 0; j < i; j++)
                ofs << element[j].number_ << " " << element[j].name_ << " " << element[j].mass_ << " " << element[j].conductivity_ << " ";
        ofs.close();
    }
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    ui->lineEdit_3->clear();
    ui->lineEdit_4->clear();
}
